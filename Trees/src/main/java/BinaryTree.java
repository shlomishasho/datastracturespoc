package main.java;

public class BinaryTree {

    // Root of Binary Tree
    private Node root;

    // Constructors
    public BinaryTree(int key)
    {
        root = new Node(key);
    }

    BinaryTree()
    {
        root = null;
    }

    Node getRoot() {
        return root;
    }

    void setRoot(Node root) {
        this.root = root;
    }

    private void inorder(Node root){
        if (root == null)
            return;
        System.out.println(root.getKey());
        inorder(root.getLeft());
        inorder(root.getRight());
    }

    private void preorder(Node root){
        if (root == null)
            return;
        inorder(root.getLeft());
        System.out.println(root.getKey());
        inorder(root.getRight());
    }

    private void postorder(Node root){
        if (root == null)
            return;
        inorder(root.getRight());
        System.out.println(root.getKey());
        inorder(root.getLeft());

    }

    void inorder(){
        inorder(root);
    }

    void preorder(){
        preorder(root);
    }

    void postorder(){
        postorder(root);
    }

    void mirrorTree(Node root){
        if (root == null)
            return;

        Node sourceLeft = root.getLeft();
        Node sourceRight = root.getRight();

        root.setRight(sourceLeft);
        root.setLeft(sourceRight);

    }

    void mirrorTree() {
        mirrorTree(root);
    }

    private int getMaxPath(Node root){
        if (root==null)
            return 0;

        if (root.getLeft() == null && root.getRight() == null)
            return root.getKey();

        if (root.getLeft() == null && root.getRight() != null)
            if (root.getRight().getKey() >=0 )
                return root.getKey() + getMaxPath(root.getRight());
            else
                return root.getKey();

        if (root.getRight() == null && root.getLeft() != null)
            if (root.getLeft().getKey() >=0 )
                return root.getKey() + getMaxPath(root.getLeft());
            else
                return root.getKey();

        if (root.getLeft().getKey() >= root.getRight().getKey())
            return root.getKey() + getMaxPath(root.getLeft());
        else
            return root.getKey() + getMaxPath(root.getRight());
    }

    int getMaxPath(){
        return getMaxPath(root);
    }
}
