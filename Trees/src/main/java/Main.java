package main.java;

public class Main {

    public static void main(String[] args) {
        // write your code here
        System.out.println("hello !!!");

        BinaryTree tree = new BinaryTree();

        /*create root*/
        tree.setRoot(new Node(1));

        /* following is the tree after above statement

              1
            /   \
          null  null     */

        tree.getRoot().setLeft(new Node(2));
        tree.getRoot().setRight(new Node(3));


        /* 2 and 3 become left and right children of 1
               1
            /     \
          2        3
        /   \     /  \
      null null null null  */


//        tree.getRoot().getLeft().setLeft(new Node(4));
//        /* 4 becomes left child of 2
//                    1
//                /       \
//               2          3
//             /   \       /  \
//            4    null  null  null
//           /   \
//          null null
//         */
//
//        tree.getRoot().getLeft().setRight(new Node(-1));
//        tree.getRoot().getLeft().getRight().setLeft(new Node(10));
//
//        tree.getRoot().getRight().setRight(new Node(-100));
//        tree.getRoot().getRight().getRight().setRight(new Node(-200));
//
//        tree.getRoot().getRight().setLeft(new Node(100000));
//        tree.getRoot().getRight().getLeft().setLeft(new Node(-100000));



        System.out.println("Inorder traversal of binary tree is ");
        tree.inorder();

        System.out.println(tree.getMaxPath());

//        System.out.println("\nPreorder traversal of binary tree is ");
//        tree.preorder();
//
//        System.out.println("\nPostorder traversal of binary tree is ");
//        tree.postorder();
//
//        tree.mirrorTree();
//        System.out.println("\nAfter mirroring, Inorder traversal of binary tree is ");
//        tree.inorder();
//
//        System.out.println("\nAfter mirroring, Preorder traversal of binary tree is ");
//        tree.preorder();
//
//        System.out.println("\nAfter mirroring, Postorder traversal of binary tree is ");
//        tree.postorder();
    }
}
